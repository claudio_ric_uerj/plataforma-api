#coding: utf-8
from flask import Flask
from blueprints.usuarios_api import usuarios_api_blueprint
from blueprints.db           import db



def create_app(config_filename=None):
    app = Flask('plataforma_api')
    if config_filename:
        app.config.from_pyfile(config_filename)

    app.register_blueprint(usuarios_api_blueprint)
    db.init_app(app)


    return app

