import os
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media_files')


MONGO_HOST = "localhost"
MONGO_PORT = 27017
MONGO_DBNAME = "test"
