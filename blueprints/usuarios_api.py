# coding:utf-8
import os, json
from werkzeug import secure_filename
from flask import (
    Blueprint, request, current_app, send_from_directory, render_template
)
from bson import ObjectId
from flask_restful import Api, Resource, url_for
from db import mongo
usuarios_api_blueprint = Blueprint('usuarios_api',__name__)
api = Api(usuarios_api_blueprint)



class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)





class UsuariosApi(Resource):

    def get(self,id=None):

        if id:
            usuarios = mongo.db.usuario.find({"id":id})
        else:
            usuarios = mongo.db.usuario.find()

        result = list(usuarios)
        result = JSONEncoder().encode(result)  #json.encode(result, cls=JSONEncoder)

        return result #json.dumps(list(usuarios),default=json_util.default) #dumps(list(usuarios))


    def post(self):
        pass


api.add_resource(UsuariosApi, '/usuarios', '/usuarios/<int:id>')

